var sh = require('shelljs');
var gulp = require('gulp');
var fs = require('fs');

var loc = {
  src: 'src/',
  dist: 'dist/',
  bin: "bin/"
};

gulp.task('compile', function () {
  if (!sh.test('-d', loc.dist)) {
    sh.mkdir(loc.dist);
  }
  if (sh.test('-d', loc.src)) {
    console.log('src is a directory');
    return sh.exec('javac ' + loc.src + '*.java -d ' + loc.dist);
  }
  return 1;
})

gulp.task('run', function () {
  if (sh.test('-d', loc.dist)) {
    sh.exec('java -cp .:dist Hello');
  }
})

gulp.task('bins', function () {
  if (!sh.test('-d', loc.bin)) {
    sh.mkdir(loc.bin);
  }
  if (sh.test('-d', loc.dist)) {
    sh.ls(loc.dist).forEach(function (classFile) {
      var basename = classFile.substring(0, classFile.lastIndexOf('.'));
      var bash_script = '#!/bin/bash\njava -cp .:' + loc.dist + ' ' + basename + ' $*';
      var bash_script_path = loc.bin + basename.toLowerCase();
      fs.writeFile(bash_script_path, bash_script, function (err) {
        if (err) throw err;
        console.log("bash script written for " + basename);
      });
      if (sh.test('-f', bash_script_path)) {
        sh.chmod('+x', bash_script_path);
      }
      var batch_script = 'java -cp "*;' + loc.dist.replace('/', '') + '" ' + basename + ' %*';
      fs.writeFile(loc.bin + basename.toLowerCase() + '.bat', batch_script, function (err) {
        if (err) throw err;
        console.log("batch script written for " + basename);
      });
    })
  }
})
