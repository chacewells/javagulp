import java.util.stream.Stream;

public class Reverse {
  public static void main(String[] args) {
    Stream.of(args).map(Reverse::reverse).forEach(System.out::println);
  }

  public static String reverse(String str) {
    StringBuilder sb = new StringBuilder();
    for (int i = str.length() - 1; i > -1; --i) {
      sb.append(str.charAt(i));
    }
    return sb.toString();
  }
}
